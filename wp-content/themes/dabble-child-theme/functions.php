<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */
function hello_elementor_child_enqueue_scripts() {
    wp_enqueue_style(
        'hello-elementor-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        [
            'hello-elementor-theme-style',
        ],
        '1.0.0'
    );
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts', 20 );


// VOLTAGE CUSTOM
add_filter('use_block_editor_for_post','__return_false');


// 2 Oct 2021 (cnielson) - Added External Script as Request by Client
function add_async_forscript($url)
{
    if (strpos($url, '#asyncload')===false)
        return $url;
    else if (is_admin())
        return str_replace('#asyncload', '', $url);
    else
        return str_replace('#asyncload', '', $url)."' async='async"; 
}
add_filter('clean_url', 'add_async_forscript', 11, 1);

// 2 Oct 2021 (cnielson) - Chatbot Snippet

function intercom_hook_javascript() {
?>
<script data-provider="intercom">
window.intercomSettings = {app_id:"gtd5ysql",hide_default_launcher:false};
(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/gtd5ysql';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<?php
}

add_action('wp_head', 'intercom_hook_javascript');

// 2 Oct 2021 (cnielson) - Yoast is overwritting this but I'm leaving this here for now in case that changes
// add_filter( 'document_title_separator', 'document_title_separator', 999 );
// function document_title_separator( $sep ) {

//     $sep = "|";

//     return $sep;

// }

add_action( 'elementor/query/fix_pagination', function( $query ) {
    $paged = ( $query->query_vars['paged'] ) ? $query->query_vars['paged'] : 1;
    $query->set('paged',$paged);
} );

// require_once( get_stylesheet_directory() . '/inc/class-primary-nav-walker.php' );

// require_once( get_stylesheet_directory() . '/inc/class-primary-nav-walker-mobile.php' );

// require_once( get_stylesheet_directory() . '/inc/custom-elementor-locations.php' );

// require_once( get_stylesheet_directory() . '/inc/woocommerce.php' );

function load_elementor_widgets() {
    require_once( get_stylesheet_directory() . '/inc/class-elementor-product-slider.php' );
    \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Elementor_Product_Slider() );
}
// add_action( 'init', 'load_elementor_widgets' );
