// console.log('kinetix/assets/js/product-slider.js');
import 'slick-carousel';
let $sliders = $( '.js-product-slider' );
$sliders.each( function() {
	let $slide = $( this ),
		layout = $slide.data( 'layout' ),
		slideOptions = {
			dots: false,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			mobileFirst: true
		};
	if ( layout === 'dt-4-slide' ) {
		slideOptions.responsive = [
			{
				breakpoint: 768,
				settings: { slidesToShow: 4 }
			}
		];
	}
	if ( $slide.hasClass( 'pdp-upsells__slider--pdp-page' ) ) {
	  slideOptions.responsive = [
			{
				breakpoint: 1024,
				settings: { slidesToShow: 3 }
			},
			{
				breakpoint: 640,
				settings: { slidesToShow: 2 }
			}
		];
	}
	$slide.slick( slideOptions );
} );
