<?php

// Remove related products
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

// Remove upsells
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );

// Remove tabs
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs' );

// Move product meta to above the title
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 4 );

// Remove breadcrumb
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

// Display ACF description on the PDP
function __project_pdp_description() {
	echo '<div class="pdp-details__description">' . get_field('product_description') . '</div>';
}
add_action( 'woocommerce_single_product_summary', '__project_pdp_description', 6 );

// Display short description on product cards
function __project_excerpt_in_product_archives() {
	echo '<p class="short-description">' . get_the_excerpt() . '</p>';
}
add_action( 'woocommerce_after_shop_loop_item_title', '__project_excerpt_in_product_archives', 40 );

// Fix for LOTS of variations
// http://www.iconwebsolutions.info/disable-unavailable-variations-in-drop-down-list-of-variable-product-woocommerce/
function __project_wc_ajax_variation_threshold( $qty, $product ) {
	return 100;
}
add_filter( 'woocommerce_ajax_variation_threshold', '__project_wc_ajax_variation_threshold', 100, 2 );

// Move notices on the single product page
remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices');

/**
 * Include woocommerce fragments for cart total in header
 */
function __project_header_cart_icon_fragment( $fragments ) {
	ob_start();
	
	get_template_part( 'template-parts/cart-icon' );
	
	$fragments['.header__cart-icon'] = ob_get_clean();
	return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', '__project_header_cart_icon_fragment' );

// Add the shop sidebars
function __project_widgets_init() {
	
	register_sidebar( array(
		'name'          => 'Woocommerce Sidebar',
		'id'            => 'woocommerce_sidebar',
		'description'   => 'Shop sidebar',
		'before_widget' => '<div class="accordion__row"><button data-hamburger class="hamburger hamburger--plus-minus" type="button" data-toggle="collapse" data-target="#%1$s-panel" aria-expanded="false" aria-controls="#%1$s-panel"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button><div id="%1$s" class="widget %2$s accordion__header" data-toggle="collapse" data-target="#%1$s-panel" aria-expanded="false" aria-controls="%1$s-panel">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="woocommerce-shop__filters-title">',
		'after_title'   => '</h2></div>',
	) );

	register_sidebar( array(
		'name'          => 'Woocommerce Topbar',
		'id'            => 'woocommerce_topbar',
		'description'   => 'Topbar for active filters',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="woocommerce-shop__active-filters-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', '__project_widgets_init' );

// Remove the functionality where WC takes you directly to a PDP when a single result is returned
add_filter( 'woocommerce_redirect_single_search_result', '__return_false' );
