import( 'bootstrap' );

// import 'header';
import 'lazysizes';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes/plugins/rias/ls.rias';

// add simple support for background images:
document.addEventListener( 'lazybeforeunveil', function( e ) {
	var bg = e.target.getAttribute( 'data-bg' );
	if ( bg ) {
		e.target.style.backgroundImage = 'url(' + bg + ')';
	}
} );

$( document ).ready( function() {

	if ( $( '.js-product-slider' ).length ) {
		import( 'product-slider' );
	}
} );
