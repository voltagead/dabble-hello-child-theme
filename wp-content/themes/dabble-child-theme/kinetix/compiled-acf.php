<?php

// if( function_exists('acf_add_local_field_group') ):

// acf_add_local_field_group(array(
// 	'key' => 'group_309v8dh473j21',
// 	'title' => 'Menu Media',
// 	'fields' => array(
// 		array(
// 			'key' => 'field_102938d83j4k3d',
// 			'label' => 'Has Media',
// 			'name' => 'has_media',
// 			'type' => 'true_false',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'message' => '',
// 			'default_value' => 0,
// 			'ui' => 0,
// 			'ui_on_text' => '',
// 			'ui_off_text' => '',
// 		),
// 		array(
// 			'key' => 'field_12kvd0385njet',
// 			'label' => 'Heading',
// 			'name' => 'heading',
// 			'type' => 'text',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => array(
// 				array(
// 					array(
// 						'field' => 'field_102938d83j4k3d',
// 						'operator' => '==',
// 						'value' => '1',
// 					),
// 				),
// 			),
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'default_value' => '',
// 			'placeholder' => '',
// 			'prepend' => '',
// 			'append' => '',
// 			'maxlength' => '',
// 		),
// 		array(
// 			'key' => 'field_20398dieh5841',
// 			'label' => 'Subheading',
// 			'name' => 'subheading',
// 			'type' => 'text',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => array(
// 				array(
// 					array(
// 						'field' => 'field_102938d83j4k3d',
// 						'operator' => '==',
// 						'value' => '1',
// 					),
// 				),
// 			),
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'default_value' => '',
// 			'placeholder' => '',
// 			'prepend' => '',
// 			'append' => '',
// 			'maxlength' => '',
// 		),
// 		array(
// 			'key' => 'field_dk9303l5j381l',
// 			'label' => 'Image',
// 			'name' => 'image',
// 			'type' => 'image',
// 			'instructions' => '414px wide',
// 			'required' => 0,
// 			'conditional_logic' => array(
// 				array(
// 					array(
// 						'field' => 'field_102938d83j4k3d',
// 						'operator' => '==',
// 						'value' => '1',
// 					),
// 				),
// 			),
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'return_format' => 'array',
// 			'preview_size' => 'medium',
// 			'library' => 'all',
// 			'min_width' => '',
// 			'min_height' => '',
// 			'min_size' => '',
// 			'max_width' => '',
// 			'max_height' => '',
// 			'max_size' => '',
// 			'mime_types' => '',
// 		),
// 		array(
// 			'key' => 'field_10vodi3941293',
// 			'label' => 'CTA',
// 			'name' => 'cta',
// 			'type' => 'link',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => array(
// 				array(
// 					array(
// 						'field' => 'field_102938d83j4k3d',
// 						'operator' => '==',
// 						'value' => '1',
// 					),
// 				),
// 			),
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'return_format' => 'array',
// 		),
// 	),
// 	'location' => array(
// 		array(
// 			array(
// 				'param' => 'nav_menu_item',
// 				'operator' => '==',
// 				'value' => 'location/primary',
// 			),
// 		),
// 	),
// 	'menu_order' => 0,
// 	'position' => 'normal',
// 	'style' => 'default',
// 	'label_placement' => 'top',
// 	'instruction_placement' => 'label',
// 	'hide_on_screen' => '',
// 	'active' => true,
// 	'description' => '',
// ));

// endif;


// acf_add_local_field_group( array(
// 	'key' => 'kinetix_page_builder_group',
// 	'title' => 'Kinetix',
// 	'fields' => array(
// 		array(
// 			'key' => 'kinetix_page_builder',
// 			'label' => 'Page Builder',
// 			'name' => 'kinetix_page_builder',
// 			'type' => 'flexible_content',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'button_label' => 'Add Component',
// 			'min' => '',
// 			'max' => '',
// 			'layouts' => array(),
// 		)
// 	),
// 	'location' => array(
// 		array(
// 			array(
// 				'param' => 'page_template',
// 				'operator' => '==',
// 				'value' => 'kinetix/template-kinetix-page-builder.php',
// 			),
// 		),
// 	),
// 	'menu_order' => 0,
// 	'position' => 'normal',
// 	'style' => 'seamless',
// 	'label_placement' => 'top',
// 	'instruction_placement' => 'label',
// 	'hide_on_screen' => array(
// 		0 => 'the_content',
// 		1 => 'excerpt',
// 		2 => 'discussion',
// 		3 => 'comments',
// 		4 => 'format',
// 		5 => 'featured_image',
// 		6 => 'send-trackbacks',
// 	),
// 	'active' => 1,
// 	'description' => '',
// ) );



// if( function_exists('acf_add_options_page') ) {

// 	acf_add_options_page( array(
// 		'page_title' 	=> 'Theme General Settings',
// 		'menu_title'	=> 'Theme Settings',
// 		'menu_slug' 	=> 'theme-general-settings',
// 		'capability'	=> 'edit_posts',
// 		'redirect'		=> false
// 	) );

// 	acf_add_local_field_group(array(
// 		'key' => 'group_5f534cea0506p',
// 		'title' => 'Header',
// 		'fields' => array(
// 			array(
// 				'key' => 'field_5f4d9r8s1913c',
// 				'label' => 'Announcement Banner',
// 				'name' => 'announcement_banner',
// 				'type' => 'text',
// 				'instructions' => '',
// 				'required' => 0,
// 				'conditional_logic' => 0,
// 				'wrapper' => array(
// 					'width' => '',
// 					'class' => '',
// 					'id' => '',
// 				),
// 				'default_value' => 'Announcement Banner',
// 				'placeholder' => '',
// 				'prepend' => '',
// 				'append' => '',
// 				'maxlength' => '',
// 			),
// 		),
// 		'location' => array(
// 			array(
// 				array(
// 					'param' => 'options_page',
// 					'operator' => '==',
// 					'value' => 'theme-general-settings',
// 				),
// 			),
// 		),
// 		'menu_order' => 0,
// 		'position' => 'normal',
// 		'style' => 'default',
// 		'label_placement' => 'top',
// 		'instruction_placement' => 'label',
// 		'hide_on_screen' => '',
// 		'active' => true,
// 		'description' => '',
// 	));
// }
