/**
 * Define extensions, paths
 */
var concat = require( 'gulp-concat' ),
	cssnano = require( 'gulp-cssnano' ),
	emoji = require( 'node-emoji' ),
	glob = require( 'glob' ),
	gulp = require( 'gulp' ),
	clean = require( 'gulp-clean' ),
	gutil = require( 'gulp-util' ),
	imagemin = require( 'gulp-imagemin' ),
	livereload = require( 'gulp-livereload' ),
	notify = require( 'gulp-notify' ),
	autoprefixer = require( 'gulp-autoprefixer' ),
	rename = require( 'gulp-rename' ),
	replace = require( 'gulp-replace' ),
	sass = require( 'gulp-sass' ),
	sassGlob = require( 'gulp-sass-glob' ),
	sort = require( 'gulp-sort' ),
	sourcemaps = require( 'gulp-sourcemaps' ),
	stylelint = require( 'gulp-stylelint' ),
	stylelintFormatter = require( 'stylelint-formatter-pretty' ),
	path = require( 'path' ),
	webpackStream = require( 'webpack-stream' );

var	paths = {
	WATCH_SCSS: [
		'assets/scss/*.scss',
		'assets/scss/**/*.scss',
		'components/**/scss/*.scss'
	],
	WATCH_PHP: [
		'components/**/acf*.php',
		'acf/**/*.php'
	],
	WATCH_JS: [
		'assets/js/**/*.js',
		'components/**/js/*.js'
	],
	SCSS_ENTRY: 'assets/scss/styles.scss',
	IMG: 'assets/img/',
	BUILD: 'assets/build',
	DIST: 'assets/dist'
};

var webpackHash = '';


/**
 * Helper Tasks
 */
function setWebpackEnv( env ) {
	return process.env.NODE_ENV = env;
}


/**
 * Development Tasks
 */


// Running Webpack
function webpackJs( watch, done ) {
	var shouldWatch = ( watch === 'true' ) ? true : false,
		webpackConfig = require( './devconfig/webpack.config.js' );

	webpackConfig.watch = shouldWatch;
	return gulp.src( './assets/js/site.js' )
		// Pass in "require( 'webpack' )" to use the same version of webpack as the rest of the build
	    .pipe( webpackStream( webpackConfig, require( 'webpack' ), function ( err, stats ) {
				if ( err ) {
					console.log( 'webpackJs Error: ', err );
				}
				if ( stats ) {
					console.log( stats.toString( webpackConfig.stats ) );

					if ( ( webpackHash !== stats.hash ) && shouldWatch ) {
						livereload.reload( 'Browser' );
					}
					webpackHash = stats.hash;
				}
				if ( shouldWatch ) {
					notify( 'JS compiled with webpack! ' + emoji.get( 'thumbsup' ) );
				}
			} ) )
	    .pipe( gulp.dest( ( process.env.NODE_ENV === 'dist' ) ? paths.DIST : paths.BUILD ) );
}


// Lint Sass files
function lintSass() {
	return gulp.src( paths.WATCH_SCSS )
		.pipe( stylelint( {
				configFile: './devconfig/stylelint.config.js',
				failAfterError: false,
				fix: true, // This options doesn't seem to be working
				reporters: [ {
					formatter: stylelintFormatter,
					console: true
				} ]
			} ) );
}

// Compile Sass files
function compileSass() {
	return gulp.src( paths.SCSS_ENTRY )
		.pipe( sourcemaps.init( { loadMaps: true, largeFile: true } ) )
		.pipe( sassGlob() )
		.pipe( sass({ outputStyle: 'compressed' })
			.on( 'error', notify.onError( {
				message: 'Womp womp. Sass failed. Check console for errors. ' + emoji.get( 'poop' )
			} ) )
			.on( 'error', sass.logError ))
		.pipe( autoprefixer() )
		.pipe( sourcemaps.write( '../maps' ) )
		.pipe( gulp.dest( paths.BUILD ) )
		.pipe( livereload() )
		.pipe( notify( 'Sass successfully compiled! ' + emoji.get( 'thumbsup' ) ) );
}

// Concatinate Component ACF Fields
function compileComponentACF() {
	return gulp.src( paths.WATCH_PHP )
		.pipe( sort({
			asc: false
		}) )
		.pipe( replace( /^\s*<\?php|\?>\s*$/g, '' ) ) // Strip Opening/Ending PHP Tags
		.pipe( concat( 'compiled-acf.php' ) )
		.pipe( replace( /^/, '<?php' ) ) // Insert single PHP Tag
		.pipe( gulp.dest( './' ) )
		.pipe( notify( 'Components successfully compiled! ' + emoji.get( 'thumbsup' ) ) );
}

/**
 * Production Tasks
 */

// Concatenate, minify, move CSS/SASS files
function buildMinCss() {
	return gulp.src( [ paths.BUILD + '/*.css', paths.BUILD + '/**/*.css' ] )
		.pipe( rename({ suffix: '.min' }) )
		.pipe( cssnano({ reduceIdents: false, autoprefixer: false }) )
		.pipe( gulp.dest( paths.DIST ) );
}

// Optimize images
function compressImgs() {
	return gulp.src( [ paths.IMG + '*.*', paths.IMG + '**/*.*' ] )
		.pipe( imagemin( [
			imagemin.gifsicle( { interlaced: true } ),
			imagemin.jpegtran( { progressive: true } ),
			imagemin.svgo( {
				plugins: [
					{ removeViewBox: false }
				]
			} )
		] ) )
		.pipe( gulp.dest( paths.IMG ) );
}

// Delete the dist folder
function cleanDist() {
	return gulp.src( paths.DIST, {read: false} )
		.pipe( clean() );
}

/**
 * Build Task Functions
 */
function watch() {
	notify( 'Watching for changes... ' + emoji.get( 'eyes' ) ).write( '' );
	setWebpackEnv( 'build' );
	webpackJs( 'true' );
	gulp.watch( paths.WATCH_SCSS, gulp.series( gulp.parallel( lintSass, compileSass ) ) );
	gulp.watch( paths.WATCH_PHP, compileComponentACF );
	livereload.listen();
}

function buildTasks( done ) {
	setWebpackEnv( 'build' );
	gulp.series(
		gulp.parallel(
			compileSass,
			compileComponentACF,
			webpackJs
		)
	)();
	done();
}

function stageTasks( done ) {
	notify( 'Compiling for staging... ' + emoji.get( 'pray' ) ).write( '' );
	setWebpackEnv( 'dist' );
	gulp.series(
		cleanDist,
		compileSass,
		gulp.parallel(
			buildMinCss,
			webpackJs,
			compileComponentACF
		)
	)();
	done();
}

function prodTasks( done ) {
	notify( 'Compiling for production... ' + emoji.get( 'pray' ) ).write( '' );
	setWebpackEnv( 'dist' );
	gulp.series(
		cleanDist,
		compileSass,
		gulp.parallel(
			buildMinCss,
			webpackJs,
			compressImgs,
			compileComponentACF
		)
	)();
	done();
}

// Default Task
gulp.task( 'default', buildTasks );

// Build task same as deafault
gulp.task( 'build', buildTasks );

// Watch Task
gulp.task( 'watch', watch );

// Stage - minified css and js, no images
gulp.task( 'stage', stageTasks );

// Prod - minified css and js and compressed images
gulp.task( 'prod', prodTasks );
