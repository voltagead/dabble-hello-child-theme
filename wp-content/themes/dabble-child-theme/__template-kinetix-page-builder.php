<!-- @DEV renamte this file and add the 'template name' line comment to the below PHP comment to allow it to display in WP -->
<?php
/**
 *
 * @package 
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
				<div id="kinetix-page-builder">
					<?php Kinetix::get_page_builder( 'kinetix_page_builder' ); ?>
				</div>
			<?php endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
